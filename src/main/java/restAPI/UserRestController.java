package restAPI;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import models.User;
import repositry.UserRepositry;

@RestController
public class UserRestController {

  @Autowired
   UserRepositry userRep;
 
 
  
  
  
  @RequestMapping("/students")
  public List<User> retrieveAllUsers()
  {
	  
	  return (List<User>) userRep.findAll();
  }
  
 
}
