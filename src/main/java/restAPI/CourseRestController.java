package restAPI;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import models.Game;
import repositry.GameRepositry;

public class CourseRestController {

	@Autowired
		GameRepositry gameRep;
		
		
	@RequestMapping("/games")
	public List<Game> retrieveAllGames()
	{
		return (List<Game>) gameRep.findAll();
	}
}
