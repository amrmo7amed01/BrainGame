package repositry;

import org.springframework.data.repository.CrudRepository;

import models.Comment;

public interface CommentRepositry extends CrudRepository<Comment, Integer>{

}
