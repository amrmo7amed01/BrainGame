package repositry;

import org.springframework.data.repository.CrudRepository;

import models.Game;

public interface GameRepositry  extends CrudRepository<Game, Integer>{

}
