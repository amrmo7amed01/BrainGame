package repositry;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import models.User;
@Repository
public interface UserRepositry extends CrudRepository<User, Integer>{

}
