package repositry;

import org.springframework.data.repository.CrudRepository;

import models.Course;

public interface CourseRepositry extends CrudRepository<Course, Integer>{

}
