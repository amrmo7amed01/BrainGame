package repositry;

import org.springframework.data.repository.CrudRepository;

import models.Playingrecord;

public interface PlayingRecordRepositry extends CrudRepository<Playingrecord, Integer> {

}
