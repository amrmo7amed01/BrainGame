package repositry;

import org.springframework.data.repository.CrudRepository;

import models.Question;

public interface QuestionRepositry extends CrudRepository<Question, Integer> {

}
