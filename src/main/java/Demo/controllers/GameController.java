package Demo.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import Demo.models.Course;
import Demo.models.Game;
import Demo.models.Question;
import Demo.models.User;
import Demo.repositry.CourseRepositry;
import Demo.repositry.GameRepositry;
import Demo.repositry.QuestionRepositry;
import Demo.repositry.UserRepositry;

@Controller
public class GameController {
	@Autowired
	  GameRepositry gameRep;
	
	@Autowired
	  CourseRepositry courseRep;
	
	@Autowired
	 UserRepositry userRep;
	
	@Autowired
	  QuestionRepositry questionRep;
	
	
	@RequestMapping(value="/addgame/{courseID}" ,method=RequestMethod.POST)
	 public @ResponseBody Map<String, String> createGame(@PathVariable("courseID") int courseID, @RequestBody Game game)
	 {
		Map<String,String> data = new HashMap<>();

		User Teacher =  userRep.findOne(game.getTecherID());
		if(Teacher.getTeacherCode().equals("TA123"))
		{
			Course course = courseRep.findOne(courseID);
			if(course != null)
			{
				game.setCourse(course);
				gameRep.save(game);
				Game game2 = gameRep.findByGameName(game.getGameName());
				data.put("ID", game2.getGameID()+"");
				data.put("check", "true");
				return data;
				
			}
			
		
		}
		data.put("check", "false");
		return data;
		
	 }
	
	
	@RequestMapping (value = "/addquestions/{gameID}", method = RequestMethod.POST )
	 public @ResponseBody Map<String, String> addquestions(@PathVariable int gameID, @RequestBody List<Question> questions )
	 {
		Map<String,String> data = new HashMap<>();
		
		 Game game = gameRep.findOne(gameID);
		 
		 if(game != null)
		 {
			for (Question question : questions) 
			{
				question.setGame(game);
				questionRep.save(question);
			}
			data.put("check", "true");
			return data;
		 }
		
		data.put("check", "false"); 
		return data;
	 }
	

}
