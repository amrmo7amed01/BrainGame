package Demo.restAPI;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Demo.models.Question;
import Demo.repositry.GameRepositry;
import Demo.repositry.QuestionRepositry;

@RestController
public class GameRestController {

	@Autowired
	  GameRepositry gameRep;
	
	@Autowired
	   QuestionRepositry questionRep;
	
		@RequestMapping( value="/playgame/{gameID}" ,method=RequestMethod.POST)
		public @ResponseBody List<Question> playGame(@PathVariable("gameID")  int gameID)
		{
			if(gameRep.findOne(gameID)!= null)
			{
				return questionRep.findByGame(gameRep.findOne(gameID));
			}
			
		 return Collections.emptyList();
		}
}
