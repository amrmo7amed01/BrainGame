package Demo.restAPI;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Demo.models.Course;
import Demo.models.Game;
import Demo.repositry.CourseRepositry;
import Demo.repositry.GameRepositry;
@RestController
public class CourseRestController {

	@Autowired
		GameRepositry gameRep;
		
	@Autowired
		CourseRepositry courseRep;
		
	
	
	
	
	@RequestMapping("/games")
	public List<Game> retrieveAllGames(@RequestParam("coursID") int id)
	{
		Course course = courseRep.findOne(id);
		List<Game> games =new ArrayList<>();
		if(course!=null)
		{
			games =  gameRep.findByCourse(course);
		}
		return games;
	}
	
	
	
	@RequestMapping("/courses")
	public List<Course> rerieveAllCourses()
	{
		
		return (List<Course>) courseRep.findAll();
	}
}
