package Demo.repositry;

import org.springframework.data.repository.CrudRepository;

import Demo.models.Comment;

public interface CommentRepositry extends CrudRepository<Comment, Integer>{

}
