package Demo.repositry;

import org.springframework.data.repository.CrudRepository;

import Demo.models.Playingrecord;

public interface PlayingRecordRepositry extends CrudRepository<Playingrecord, Integer> {

}
