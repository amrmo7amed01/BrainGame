package Demo.repositry;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import Demo.models.Game;
import Demo.models.Question;

public interface QuestionRepositry extends CrudRepository<Question, Integer> {

	public List<Question> findByGame(Game game);
}
